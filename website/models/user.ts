import { Role } from './role';

export interface User {
  id: string;
  name: string;
  email: string;
  roles: Role[];
}

export type UserWithPassword = User & {
  password: string;
};
