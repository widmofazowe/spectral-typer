import type { NextPage } from 'next';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';

function createData(match: string, score?: string, bet?: string) {
  return { match, score, bet };
}

const rows = [
  createData('PSG vs MAD', '1 : 1', '1 : 0'),
  createData('PSG vs MAD2', '1 : 1', '1 : 0'),
  createData('PSG vs MAD3', '1 : 1', undefined),
  createData('PSG vs MAD4', undefined, undefined),
];

type Props = {
  event: string;
  round: string;
};

const Round: NextPage<Props> = ({ event, round }) => {
  return (
    <>
      <Typography variant="h4" component="h1" gutterBottom>
        {event}
        {round}
      </Typography>
      <TableContainer component={Paper}>
        <Table aria-label={round}>
          <TableHead>
            <TableRow>
              <TableCell>Match</TableCell>
              <TableCell>Score</TableCell>
              <TableCell>Bet</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row => (
              <TableRow key={row.match} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell component="th" scope="row">
                  {row.match}
                </TableCell>
                <TableCell>{row.score}</TableCell>
                <TableCell>{row.bet}</TableCell>
                <TableCell></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default Round;

export const getServerSideProps = async context => {
  const {
    params: { event, round },
  } = context;

  return {
    props: {
      event,
      round,
    },
  };
};
