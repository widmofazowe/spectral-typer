import React from 'react';
import { AppBar, Avatar, Grid, IconButton, Link, Tab, Tabs, Toolbar, Tooltip } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import NotificationsIcon from '@mui/icons-material/Notifications';
import { useUser } from '@hooks/useUser';

const lightColor = 'rgba(255, 255, 255, 0.7)';

interface Props {
  onDrawerToggle: () => void;
}

const Header: React.FC<Props> = ({ onDrawerToggle }) => {
  const { data: user } = useUser();

  return (
    <>
      <AppBar color="primary" position="sticky" elevation={0}>
        <Toolbar>
          <Grid container spacing={1} alignItems="center">
            <Grid sx={{ display: { sm: 'none', xs: 'block' } }} item>
              <IconButton color="inherit" aria-label="open drawer" onClick={onDrawerToggle} edge="start">
                <MenuIcon />
              </IconButton>
            </Grid>
            <Grid item xs />
            <Grid item>
              {/* <Link
                href="/"
                variant="body2"
                sx={{
                  textDecoration: 'none',
                  color: lightColor,
                  '&:hover': {
                    color: 'common.white',
                  },
                }}
                rel="noopener noreferrer"
                target="_blank"
              >
                Go to docs
              </Link> */}
            </Grid>
            <Grid item>
              <Tooltip title="Alerts • No alerts">
                <IconButton color="inherit">{/* <NotificationsIcon /> */}</IconButton>
              </Tooltip>
            </Grid>
            <Grid item>
              <IconButton color="inherit" sx={{ p: 0.5 }}>
                <Avatar alt={user?.name} />
              </IconButton>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <AppBar component="div" position="static" elevation={0} sx={{ zIndex: 0 }}>
        <Tabs value={0} textColor="inherit">
          <Tab label="Split 1" />
          <Tab label="Split 2" />
          <Tab label="Play Off" />
          <Tab label="Final" />
        </Tabs>
      </AppBar>
    </>
  );
};
export default Header;
