import { useQuery } from 'react-query';
import { User } from '@models/user';
import { useAuthToken } from './useAuthToken';
import { useApiHttpService } from './useApiHttpService';

type AuthTokenDecoded = User;

export const useUser = () => {
  const authToken = useAuthToken();
  const httpService = useApiHttpService();

  return useQuery(
    ['current-user'],
    async () => {
      const { data } = await httpService.get('/user');
      return data;
    },
    {
      enabled: authToken !== undefined,
    },
  );
};
